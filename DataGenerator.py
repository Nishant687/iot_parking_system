from faker import Faker
import json
import random
import time
from confluent_kafka import Producer

#initialization of faker library
fake = Faker()

#function for license plate generation
def generate_license_plate():
    return str(random.randint(1000, 9999))

#function for generation of exit time values
def generate_exit_time(vehicle_type):
    if random.random() < 0.5: #randomly return None for 50% of the cases  #can be changed as per the requirement
        return None
    else:
        return fake.date_time_this_decade()

#function for generation of parking slot
def generate_parking_data(topic, slot):
    
    vehicle_type = random.choice(['car', 'bicycle']) #randomly selection of vehicles
    
    #to set license plate for cars only and exclude bicycles
    if vehicle_type == 'car':
        license_plate = generate_license_plate()
    else:
        license_plate = None
    
    #generation of entry time and exit time for the vehicle
    entry_time = fake.date_time_this_decade()
    exit_time = generate_exit_time(vehicle_type)

    #construct the parking data dictionary
    data = {
        'vehicle_type': vehicle_type,
        'license_plate': license_plate,
        'parking_slot': fake.random_int(min=1, max=600),
        'entry_time': entry_time.isoformat(),
        'exit_time': exit_time.isoformat() if exit_time else None,
    }

    return data

#function to send data to Kafka topic using the provided producer
def send_to_kafka(producer, topic, data):
    producer.produce(topic, json.dumps(data).encode('utf-8'))
    producer.flush()

#main function to generate parking data and send it to Kafka
def main():
    # Kafka producer configuration
    producer_conf = {'bootstrap.servers': 'localhost:9092'}
    producer = Producer(producer_conf)

    try:
        data_count = 5000  #total number of data entries to generate
        for i in range(data_count):
            #Generate and send parking data for slot 1
            slot_1_data = generate_parking_data("parkDataSlot1", slot=1)
            send_to_kafka(producer, "parkDataSlot1", slot_1_data)
            print(f"Data generated and sent to parkDataSlot1: {slot_1_data}")

            #Generate and send parking data for slot 2
            slot_2_data = generate_parking_data("parkDataSlot2", slot=2)
            send_to_kafka(producer, "parkDataSlot2", slot_2_data)
            print(f"Data generated and sent to parkDataSlot2: {slot_2_data}")

            time.sleep(1)  # Introduce a 1-second delay between entries
        print(f"Total of {data_count} data entries sent to each topic.")
    except KeyboardInterrupt:
        pass
    finally:
        producer.flush()
        producer.close()

#entry point of the script
if __name__ == "__main__":
    main()
